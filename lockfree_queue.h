#pragma once

namespace fwx
{

template<typename T>
class LockFreeQueue
{
	struct Node;
public:

	LockFreeQueue() = default;
	~LockFreeQueue() = default;

	LockFreeQueue(LockFreeQueue&) = delete;
	void operator=(LockFreeQueue&) = delete;
	LockFreeQueue(LockFreeQueue&&) = delete;
	void operator=(LockFreeQueue&&) = delete;

	void push_front(T&& val)
	{
		auto p = std::make_shared<Node>(std::forward<T>(val));
		p->next = head;
		while(!std::atomic_compare_exchange_weak(&head, &(p->next), p))
			{}
	}

	void pop_front()
	{
		auto p = std::atomic_load(&head);
		while(!std::atomic_compare_exchange_weak(&head, &p, p->next))
			{}
		// no need to set "head->prev" to nullptr -- it's zero-constructed
	}

	auto front()
	{
		auto p = std::atomic_load(&head);
		return p;
	}

	auto back()
	{
		auto p = std::atomic_load(&tail);
		return p;
	}

	void push_back(T&& val)
	{
		auto p = std::make_shared<Node>(std::forward<T>(val));
		p->prev = tail;
		while(!std::atomic_compare_exchange_weak(&tail, &(p->prev), p))
			{}
	}

	void pop_back()
	{
		auto p = std::atomic_load(&tail);
		while(!std::atomic_compare_exchange_weak(&tail, &p, p->prev))
			{}
		// no need to set "head->next" to nullptr -- it's zero-constructed
	}

private:
	struct Node
	{
		Node(T&& val)
			: val{ std::forward<T>(val) }
		{}
		T val{};
		std::shared_ptr<Node> next;
		std::shared_ptr<Node> prev;
	};
	std::shared_ptr<Node> head;
	std::shared_ptr<Node> tail;
};

} // namespace fwx