
#include <math.h>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>
#include <random>
#include <array>
#include <unordered_map>
#include <iostream>	// dbg

#include "./nvToolsExt.h"

#include "test.h"
#include "mailbox_array.h"
#include "lockfree_queue.h"
#include "particle.h"

static std::atomic<float> globalTime;
static volatile bool workerMustExit = false;


namespace
{
constexpr std::size_t const mailbox_size{ 2048u };
constexpr std::size_t const particles_count{ 64u };

using particle_t = fwx::Firework<particles_count>;
using store_t = std::atomic<particle_t*>;

static fwx::MailboxArray<store_t, mailbox_size> particle_data;

struct alignas(8) work_t
{
	std::size_t slot;

	work_t(std::size_t slot)
		: slot{ std::move(slot) }
	{}
	work_t(work_t&& w) noexcept
		: slot{ std::move(w.slot) }
	{}

	bool operator==(work_t const& rhs) const
	{
		return slot == rhs.slot;
	}

	bool operator!=(work_t const& rhs) const
	{
		return !(*this == rhs);
	}
};
static fwx::LockFreeQueue<work_t> work;


template<std::size_t N>
using drawable_helper_t = std::unordered_map<particle_t::DataPair, particle_t::RGBA, fwx::DataPairHasher<N>>;
using drawable_t = drawable_helper_t<particles_count>;

static std::atomic<drawable_t*> to_draw;

double const pi = std::acos(-1);
using elem_t = float;
constexpr elem_t const rads_to_degrees{ 0.0174533f };
constexpr elem_t const step{ 360 / particles_count };
constexpr elem_t const particle_fade_coeff{ 0.02f };
constexpr elem_t const particle_speedup{ 3.0f };
constexpr elem_t const firework_lifetime{ 2500.f };
struct Coeff
{
	elem_t x, y;
};
std::array<Coeff, particles_count> coeffs;

float rnd_flt()
{
	static std::mt19937 gen(std::random_device{}());
	static std::uniform_real_distribution<float> dis(0.f, 1.f);

	return dis(gen);
}
} // anonymous namespace


void test::init(void)
{
	for(auto particle = 0u; particle < particles_count; ++particle)
	{
		coeffs[particle].x = static_cast<float>(std::cos(pi / (particle * step * rads_to_degrees)));
		coeffs[particle].y = static_cast<float>(std::sin(pi / (particle * step * rads_to_degrees)));
	}
}

void test::term(void)
{
	workerMustExit = true;
}

void test::render(void)
{
	drawable_t drawables = *to_draw.load();
	to_draw.store(nullptr);
	for(auto const& draw : drawables)
	{
		for(float w = draw.first.x - 5; w < draw.first.x + 5; ++w)
		{
			for(float h = SCREEN_HEIGHT - draw.first.y - 5; h < SCREEN_HEIGHT - draw.first.y + 5; ++h)
			{
				platform::drawPoint(w, h, draw.second.r, draw.second.g,
									draw.second.b, draw.second.a);
			}
		}
	}
}

void test::update(int dt)
{
	float time = globalTime.load();
	globalTime.store(time + dt);
		
	auto overlapped_points = std::unique_ptr<drawable_t>(::new drawable_t);
	particle_t* p{};
	std::size_t id;

	auto it = work.front();
	while(it && (p = particle_data[it->val.slot].load()))
	{
		id = p->father_thread_id;
		float ttl = globalTime.load();
		if(it && it->val.slot == id && ttl >= p->ttl)
		{
			work.pop_front();
			particle_data[id].store(nullptr);
		}

		p->rgba.a -= particle_fade_coeff;

		for(auto particle = 0u; particle < particles_count; ++particle)
		{
			if((*p->p)[particle].x <= SCREEN_WIDTH || (*p->p)[particle].y <= SCREEN_HEIGHT)
			{
				(*overlapped_points)[{ (*p->p)[particle].x,
									   (*p->p)[particle].y }] = p->rgba;
			}
		}

		if(it && it->next)
			it = it->next;
		else
			break;
	}
	to_draw.store(overlapped_points.release());
}

/*
particle_t* store_data_in_mailarray(std::size_t slot, float x, float y)
{
	auto* pos = ::new particle_t::positions_t<particles_count>();
	for(auto i = 0; i < particles_count; ++i)
		(*pos)[i] = { x, y };
	auto* speed = ::new particle_t::speeds_t<particles_count>();
	for(auto i = 0; i < particles_count; ++i)
		(*speed)[i] = { rnd_flt()*particle_speedup, rnd_flt()*particle_speedup };
	float ttl = globalTime.load() + firework_lifetime;

	// MSVC-specific, for GCC look up "aligned_alloc"
	void* alloc_p = _aligned_malloc(sizeof(particle_t), fwx::firework_align_size);

	auto part = (::new (alloc_p) particle_t(pos, speed, { x, y },
		{ rnd_flt(), rnd_flt(), rnd_flt(), 1.f }, ttl, slot));

	particle_data[slot].store(part);
	return part;
}*/

void test::on_click(int x, int y)
{
	std::thread thread([x, y]()
	{
		nvtxRangePush(__FUNCTION__);

		static std::size_t id{ 0 };
		id = (id + 1) % mailbox_size;
		thread_local auto own_id = id;

		work.push_front(own_id);

		//std::cout << "click id: " << own_id << "\n"; // debug output

		auto* pos = ::new particle_t::positions_t<particles_count>();
		for(auto i = 0; i < particles_count; ++i)
			(*pos)[i] = { static_cast<float>(x), static_cast<float>(y) };
		auto* speed = ::new particle_t::speeds_t<particles_count>();
		for(auto i = 0; i < particles_count; ++i)
			(*speed)[i] = { rnd_flt()*particle_speedup, rnd_flt()*particle_speedup };
		float ttl = globalTime.load() + firework_lifetime;

		// MSVC-specific, for GCC look up "aligned_alloc"
		void* alloc_p = _aligned_malloc(sizeof(particle_t), fwx::firework_align_size);

		auto part = (::new (alloc_p) particle_t(pos, speed,
			{ static_cast<float>(x), static_cast<float>(y) },
			{ rnd_flt(), rnd_flt(), rnd_flt(), 1.f }, ttl, own_id));

		particle_data[own_id].store(part);

		while(!workerMustExit && (part = particle_data[own_id].load()))
		{
			static float lastTime = 0.f;
			float time = globalTime.load();
			float delta = time - lastTime;
			lastTime = time;

			for(auto particle = 0u; particle < particles_count; ++particle)
			{
				(*part->p)[particle].x += coeffs[particle].x * (*part->s)[particle].x;
				(*part->p)[particle].y += coeffs[particle].y * (*part->s)[particle].y;
			}

			if(delta < 10)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(10 - static_cast<int>(delta*1000.f)));
			}
		}

		delete part;	// access to inner fields is already overburdened, and we
						// cannot use std::atomic<std::shared_ptr> till C++20 ...

		//std::cout << "thread number " << own_id << " said goodbye\n"; // debug output
		nvtxRangePop();
	});
	thread.detach();

	/*
	auto thread_loop = [](float& x, float& y)
	{
		nvtxRangePush(__FUNCTION__);

		static std::size_t id{ 0 };
		id = (id + 1) % mailbox_size;
		thread_local auto own_id = id;

		work.push_front(own_id);

		//std::cout << "click id: " << own_id << "\n"; // debug output

		auto part = store_data_in_mailarray(own_id, x, y);

		x = (*part->p)[(*part->p).size() - 2].x;
		y = (*part->p)[(*part->p).size() - 2].y;

		while(!workerMustExit && (part = particle_data[own_id].load()))
		{
			static float lastTime = 0.f;
			float time = globalTime.load();
			float delta = time - lastTime;
			lastTime = time;

			for(auto particle = 0u; particle < particles_count; ++particle)
			{
				(*part->p)[particle].x += coeffs[particle].x * (*part->s)[particle].x;
				(*part->p)[particle].y += coeffs[particle].y * (*part->s)[particle].y;
			}

			if(delta < 10)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(10 - static_cast<int>(delta*1000.f)));
			}
		}

		delete part;	// access to inner fields is already overburdened, and we
						// cannot use std::atomic<std::shared_ptr> till C++20 ...

		//std::cout << "thread number " << own_id << " said goodbye\n"; // debug output
		nvtxRangePop();
	};

	float click_x = static_cast<float>(x);
	float click_y = static_cast<float>(y);

	auto thread_loop_again = thread_loop;

	std::thread thread(std::move(thread_loop), std::ref(click_x), std::ref(click_y));
	thread.detach();

	if(((x % y) * (x ^ y)) & 0x1001)	// pretending to be quite random
	{
		std::thread thread2(std::move(thread_loop_again), std::ref(click_y), std::ref(click_y));
		thread2.detach();
	}*/
}