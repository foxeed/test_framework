#pragma once

namespace fwx
{

constexpr std::size_t const firework_align_size{ 128 };

template<std::size_t N, typename elem_t = float>
struct alignas(firework_align_size) Firework
{
	struct DataPair
	{
		elem_t x, y;
		bool operator==(DataPair const& rhs) const
		{
			return x == rhs.x && y == rhs.y;
		}
	};
	struct RGBA
	{
		elem_t r, g, b, a;
	};

	template<decltype(N)>
	using positions_t = typename std::array<DataPair, N>;
	template<decltype(N)>
	using speeds_t = typename std::array<DataPair, N>;

	Firework(positions_t<N>* p, speeds_t<N>* s, DataPair coords,
		RGBA rgba, float ttl, std::size_t father_thread_id)
			: p{ std::move(p) }, s{ std::move(s) }, coords{ coords },
			  rgba{ rgba }, ttl{ ttl }, father_thread_id{ father_thread_id }
	{}

	std::shared_ptr<positions_t<N>> p;
	std::shared_ptr<speeds_t<N>> s;
	DataPair coords;
	RGBA rgba;
	float ttl;
	std::size_t father_thread_id;
};

template<std::size_t N>
struct DataPairHasher
{
	std::size_t operator()(typename Firework<N>::DataPair const& hashable) const
	{
		auto h1 = static_cast<unsigned>(hashable.x);
		auto h2 = static_cast<unsigned>(hashable.y);
		return h1 ^ h2 << 1;
	};
};

} // namespace fwx
