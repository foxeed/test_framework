#pragma once

namespace fwx
{
template<typename T, std::size_t Size>
class MailboxArray
{
public:
	MailboxArray() = default;
	~MailboxArray() = default;

	MailboxArray(MailboxArray&) = delete;
	void operator=(MailboxArray&) = delete;
	MailboxArray(MailboxArray&&) = delete;
	void operator=(MailboxArray&&) = delete;

	T const& operator[](std::size_t slot) const
	{
		return mailboxes[slot];
	}
	T& operator[](std::size_t slot)
	{
		return mailboxes[slot];
	}
private:
	std::array<T, Size> mailboxes;
};

} // namespace fwx